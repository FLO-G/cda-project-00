﻿-- Exported from QuickDBD: https://www.quickdatabasediagrams.com/
-- Link to schema: https://app.quickdatabasediagrams.com/#/d/cy0TsO
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.

CREATE DATABASE cda00_poll
    DEFAULT CHARACTER SET = 'utf8mb4';

USE cda00_poll;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
    `id` int AUTO_INCREMENT NOT NULL ,
    `name` varchar(100)  NOT NULL ,
    `created_at` datetime  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);

DROP TABLE IF EXISTS `polls`;

CREATE TABLE `polls` (
    `id` int AUTO_INCREMENT NOT NULL ,
    `title` text  NOT NULL ,
    `description` text  NOT NULL ,
    `p_key` varchar(255)  NOT NULL ,
    `created_at` datetime  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);

DROP TABLE IF EXISTS `choices`;

CREATE TABLE `choices` (
    `id` int AUTO_INCREMENT NOT NULL ,
    `label` varchar(255)  NOT NULL ,
    `fk_poll` int  NOT NULL ,
    PRIMARY KEY (
        `id`
    )
);

DROP TABLE IF EXISTS `votes`;

CREATE TABLE `votes` (
    `fk_users` int  NOT NULL ,
    `fk_polls` int  NOT NULL ,
    `fk_choices` int  NOT NULL ,
    `created_at` datetime  NOT NULL
);

ALTER TABLE `choices` ADD CONSTRAINT `fk_choices_fk_poll` FOREIGN KEY(`fk_poll`)
REFERENCES `polls` (`id`);

ALTER TABLE `votes` ADD CONSTRAINT `fk_votes_fk_users` FOREIGN KEY(`fk_users`)
REFERENCES `users` (`id`);

ALTER TABLE `votes` ADD CONSTRAINT `fk_votes_fk_polls` FOREIGN KEY(`fk_polls`)
REFERENCES `polls` (`id`);

ALTER TABLE `votes` ADD CONSTRAINT `fk_votes_fk_choices` FOREIGN KEY(`fk_choices`)
REFERENCES `choices` (`id`);

