<?php

namespace MyApp\Models;

use MyApp\Models\Database;
use PDO;

require_once('../../vendor/autoload.php');

class User extends Database
{
    protected $pdo;

    public function __construct()
    {
        $this->pdo = Database::getPdo();
    }


    /**
     * This function allows the user to resgister on the website
     *
     * @return void
     */
    public function signup()
    {

        // Vérification de l'envoi du formulaire
        if (!empty($_POST)) {

            // On vérifie que tous les champs requis sont remplis
            if (isset($_POST['nickname']) && !empty($_POST['nickname'])) {

                //Récupération des données en les protégeant
                $nickname = strip_tags($_POST['nickname']);

                // Insertion dans la BDD
                $sql = "INSERT INTO user (nickname) VALUES (:nickname)";

                $query = $this->pdo->prepare($sql);

                $query->bindValue(":nickname", $nickname, PDO::PARAM_STR);

                $query->execute();
            } else {

                $_SESSION['erreur'] = "Le formulaire est incomplet";
            }
        }
    }

    /**
     * This function allows the user to log on the website
     *
     * @return void
     */
    public function login()
    {
        // Vérification de l'envoi du formulaire
        if (!empty($_POST)) {
            
            // On vérifie que tous les champs requis sont remplis
            if (isset($_POST['nickname']) && !empty($_POST['nickname'])) {

                // On vérifie que l'identifiant existe dans la base de donnée
                $query = $this->pdo->prepare('SELECT * FROM user WHERE nickname = :nickname');
                $query->bindValue(":nickname", $_POST['nickname'], PDO::PARAM_STR);
                $query->execute();

                $user = $query->fetch();

                if (!$user) {

                    header('Location: ../index.php');
                    die();
                    // $_SESSION['erreur'] = "Cet identifiant n'existe pas";
                }

                // On stocke les informations de User dans la super globale SESSION
                $_SESSION['id'] = $user['id'];
                $_SESSION['nickname'] = $user['nickname'];

                header('Location: ../controllers/cr_poll.php');

                exit;
            }
        }

    }

    /**
     * This function allows the user to disconnect to the website
     *
     * @return void
     */
    public function logout()
    {

        session_start();

        unset($_SESSION['id']);
    }
}
