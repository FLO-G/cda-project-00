<?php

namespace MyApp\Models;

use PDO;
use PDOException;

/**
 * Return the database connect
 *
 * @return PDO
 */
class Database
{
    public static function getPdo(): PDO
    {

        try {

            $pdo = new PDO("mysql:host=localhost;dbname=cda04_poll;charset=utf8", "root", "", [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ]);

            // Connection test:
            // echo 'Connecting check Ok';

        } catch (PDOException $e) {

            die($e->getMessage("Connection failed"));
        }

        return $pdo;
    }
}