<?php

namespace MyApp\Models;

use MyApp\Models\Database;
use PDO;

require_once('../../vendor/autoload.php');

class Poll extends Database
{
    protected $pdo;

    public function __construct()
    {
        $this->pdo = Database::getPdo();
    }

    /**
     * This function allows the admin to create a new article
     *
     * @return void
     */
    public function create()
    {
        if ($_POST) {
            if (isset($_POST['title']) && !empty($_POST['title']) && isset($_POST['description']) && !empty($_POST['description'])) {

                // On nettoie les données envoyées
                $title = strip_tags($_POST['title']);
                $description = strip_tags($_POST['description']);

                $sql = 'INSERT INTO poll SET title = :title, description = :description';

                $query = $this->pdo->prepare($sql);

                $query->bindValue(':title', $title, PDO::PARAM_STR);
                $query->bindValue(':description', $description, PDO::PARAM_STR);

                $query->execute();

                $_SESSION['message'] = "Sondage créé avec succés";

                header('Location: ../controllers/poll.php');

                exit;

            } else {
                $_SESSION['erreur'] = "Le formulaire est incomplet";
            }
        }
    }


    public function read()
    {

        if (isset($_GET['id']) && !empty($_GET['id'])) {

            $id = strip_tags($_GET['id']);

            $sql = 'SELECT * FROM poll WHERE id = :id;';

            $query = $this->pdo->prepare($sql);

            $query->bindValue(':id', $id, PDO::PARAM_INT);

            $query->execute(['id'=>$id]);

            $poll = $query->fetch();

            var_dump($poll);

            if (!$poll) {
                $_SESSION['erreur'] = "Cet id n'existe pas";
                header('Location: ../controllers/cr_poll.php');
            }
        } else {
            $_SESSION['erreur'] = "URL invalide";
            header('Location: ../controllers/cr_poll.php');
        }

        return $poll;
    }
}
