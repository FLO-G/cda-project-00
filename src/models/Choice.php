<?php

namespace MyApp\Models;

use MyApp\Models\Database;
use PDO;

require_once('../../vendor/autoload.php');

class Choice extends Database {

    // les attributs
    
    //
    public string $label;

    //
    public string $etiquette;

    //
    public PDO $pdo;

    //
    public $query;

    // =========================================
    // le constructeur

    public function __construct(string $label, $etiquette) {
        $this->label = $label;
        $this->etiquette = $etiquette;

        $this->pdo = Database::getPdo();
        $this->query = $this->pdo->prepare("INSERT INTO Choice (label, etiquette) VALUES (:label, :etiquette)");
    }

    // =========================================
    // les getters / setters

    // l'attribut label: get
    public function getLabel(): string {
        return $this->label;
    }

    // l'attribut label: set
    public function setLabel(string $label): void {
        $this->label = $label;
    }

    // l'attribut etiquette: get
    public function getEtiquette(): string {
        return $this->etiquette;
    }

    // l'attribut etiquette: set
    public function setEtiquette(string $etiquette): void {
        $this->etiquette = $etiquette;
    }

    // methodes persos
    
    // bind les parametres de la requete avec les valeurs associées
    // puis execute la requete
    public function test() {
        try {
            $this->query->bindValue(":label", $this->label, PDO::PARAM_STR);
            $this->query->bindValue(":etiquette", $this->etiquette, PDO::PARAM_STR);
            $this->query->execute();
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
        }   
    }

    // verif et insert° ds la DB de la valeur de l'attribut label
    public function insertDB() {
        if(!empty($_POST)) {
            // echo "<br>";
            // echo "Affichage de var_dump(S_POST) depuis insertDb()";
            // var_dump($_POST);
            // echo "<br>";

            // echo "Affichage de S_POST[question] depuis insertDB()";
            // echo "<br>";
            // echo $_POST['question'];
            // echo "<br>";

            // obligé d'afficher la valeur de l'attribut
            // label de chacune des instances pr pvoir les differencier
            // echo "Affichage de la valeur de chacune des instances";
            // echo "<br>";
            // echo $this->getLabel();
            // echo "<br>";

            echo "S_POST n'est pas vide";
            echo "<br>";
        } else {
            echo "S_POST est vide";
            echo "<br>";
        }
    }
}

function validate_and_sanitize_input(string $input) {
    // Perform validation and sanitization on the input
    // For example, strip tags and special characters, 
    // and check that the input is not empty
    $input = trim($input);
    $input = strip_tags($input);
    $input = htmlspecialchars($input);
    if(empty($input)) {
        throw new Exception('Invalid input: '.$input);
    }
    return $input;
}

// emplacement original, implementé en methode ds le class
/*
try {
    $choices = [];
    for($i = 0; $i < $_POST['nbreChoix']; $i++) {
        $label = validate_and_sanitize_input($_POST['c'.($i+1)]);
        $choices[] = new Choice($label);
        echo "echo depuis choice.php: ".$choices[$i]->getLabel();
        echo "<br>";
    }
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
 */
