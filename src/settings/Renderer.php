<?php

namespace MyApp\Settings;

class Renderer
{
    public function render(string $path, $route, array $var = []): void
    {
    
        extract($var);
    
        ob_start();
        require_once('../' . $path . '.html.php');
        $pageContent = ob_get_clean();
    
        require_once($route . '/public/layouts/layout.html.php');
    }
}