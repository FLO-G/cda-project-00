<?php

session_start();

require_once('../../vendor/autoload.php');

$render = new \MyApp\Settings\Renderer;

$poll = new \MyApp\Models\Poll;

$polls = $poll->read();

$pageTitle = $polls['title'];

$render->render('../public/templates/poll', '../..', compact('pageTitle'));