<?php

session_start();

require_once('../../vendor/autoload.php');

$render = new \MyApp\Settings\Renderer;

$pageTitle = "Nouveau sondage";

$render->render('../public/templates/cr_poll', '../..', compact('pageTitle'));