<?php

require_once('../../vendor/autoload.php');

$user = new \MyApp\Models\User;

$user->logout();

header('Location: ../index.php');