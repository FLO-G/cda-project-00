<?php session_start(); ?>

<main class="home">
  <header>
    <h1>Bienvenue sur notre site de sondage</h1>
  </header>

  <a href="../../public/controllers/logout.php">Déconnexion</a>

  <section class="container">
    <section class="sect-1">
      <form class="signup" action="controllers/signup.php" method="POST">
        <h2>Inscrivez-vous</h2>
        <h4>Pour créer ou participer à un sondage:</h4>
        <input type="text" name="nickname" id="nickname" placeholder="Saisir un identifiant *" required>
        <button type="submit" name="signup">Valider</button>
      </form>
    </section>

    <section class="sect-2">
      <form class="login" action="controllers/login.php" method="POST">
        <h2>Déjà inscris?</h2>
        <h4> Connectez-vous:</h4>
        <input type="text" name="nickname" id="nickname" placeholder="Entrer un identifiant *" required>
        <button type="submit" name="login">Valider</button>
      </form>
    </section>
  </section>

</main>