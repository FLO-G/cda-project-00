<main class="poll-vote">
  <h2>Bienvenue <?= $_SESSION['nickname'] ?></h2>
  
  <section class="container">
    <form class="create-poll" action="../controllers/crpoll.php" method="POST">
      <h2>Créer votre sondage</h2>
      <label for="title">Intitulé du sondage</label>
      <input type="text" name="title" id="title" placeholder=" *" required>
      <textarea name="description" id="description" placeholder="Description du sondage (facultatif)"></textarea>
      <label for="nbreChoix">Indiquer le nombre de choix à intégrer au vote:</label>
      <input type="number" name="nbreChoix" id="nbreChoix" min="2" max="10" placeholder="minimum 2 choix" required>
      <button type="submit" name="create">Créer le sondage</button>
    </form>
    
    <form class="part-poll" action="#" method="POST">
      <h2>Participer à un sondage</h2>
      <input type="text" name="" id="" placeholder="Entrer une clé *" required>
      <button type="submit" name="participate">Participer</button>
    </form>
    
  </section>
</main>